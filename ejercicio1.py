#__autora__ : __"Ingrid Guaraca"__
#__email__ : __"ingrid.guaraca@unl.edu.ec"__
# Descargar una copia de un archivo www.py4e.com/code3/romeo.txt. Escribir un programa para abrir el archivo romeo.txt
# y leerlo línea por línea. Para cada línea, dividir la línea en una lista de palabras utilizando la función split.
# Para cada palabra, revisar si la palabra ya se encuentra previamente en la lista. Si la palabra no está en la lista,
# agregarla a la lista. Cuando el programa termine, ordenar e imprimir las palabras resultantes en orden alfabético.

archivo = input("Ingresar nombre de archivo: ")
try:
    arch = open(archivo)
except:
    print("No se puede abrir el archivo", archivo)
    exit()
counts = dict()
for l in arch:
    print (l.split())

