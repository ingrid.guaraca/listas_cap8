#__autora__ : __"Ingrid Guaraca"__
#__email__ : __"ingrid.guaraca@unl.edu.ec"__
# Escribir un programa para leer a través de datos de una bandeja de entrada de correo y cuando encuentres una línea
# que comience con “From”, dividir la línea en palabras utilizando la función split. Estamos interesados en quién
# envió el mensaje, lo cual es la segunda palabra en las líneas que comienzan con From. Tendrás que analizar la línea
# From e imprimir la segunda palabra de cada línea From, después tendrás que contar el número de líneas From (no
# incluir From:) e imprimir el total al final.

archivo = input("Ingresa un nombre de archivo: ")
arch = open(archivo)
cont = 0
for l in arch:
    if not l.startswith('From'):
        continue
    if l.startswith('From:'):
        continue
    else:
        l = l.split()
        l = l[1]
        print(l)
    cont += 1
print("Hay",cont,"lineas en el archivo con la palabra From al inicio")